# Workflow (End of Life)

*⚠️ Workflow is not maintained anymore! Feel free to fork this project, or please consider using the ActivityWatch web interface, or any other alternative.* 

A screen time monitor app for Linux. Needs ActivityWatch to be installed (see below).

![Screenshot](https://i.imgur.com/Uk3XUQU.png)

## Installing


First of all, **you need to [download and install ActivityWatch](https://activitywatch.readthedocs.io/en/latest/getting-started.html) following the instructions on their website.**

Then you can proceed to install Workflow in the way that suits you best:

- The easiest way to install Workflow is from Flathub, for all Flatpak-enabled distributions.

<a href='https://flathub.org/apps/details/com.gitlab.cunidev.Workflow'><img width='240' alt='Download on Flathub' src='https://flathub.org/assets/badges/flathub-badge-en.png'/></a>
- ...or build a local Flatpak yourself from source by cloning this repo into GNOME Builder.

- Alternatively, you can install it from source using the Meson build tool:

```
git clone https://gitlab.com/cunidev/workflow
cd workflow
meson build
ninja -C build
```

- or run it without installing as a Python module from the source, provided you have all the needed Python Gtk/Gobject dependencies already installed:
```
git clone https://gitlab.com/cunidev/workflow
cd workflow
python3 -m workflow
```

## Bugs

Probably many. We are still at 0.1.x and there is still a long way to go for Workflow. Feel free to report any in the Issues section of GitLab

## Donations

As for most small open-source projects, the best donation you can give is in time by helping fixing bugs and adding new, shiny features. Contributions and patches (possibly discussed in advance by opening an issue).

However, if you'd really like [to buy me a coffee or a beer](https://paypal.me/tranquillini) as an aid to survive my next exam session, don't forget to thank [ActivityWatch developers](https://activitywatch.net/donate/) too, as Workflow is just a small UI built upon their work.