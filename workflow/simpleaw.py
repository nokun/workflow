# SimpleAW - a simple ActivityWatch REST client

import urllib.request
import json
from traceback import print_exc
from . import timeutils
from datetime import datetime, date, timedelta

class SimpleAW:

    BASE_AW_URL = "http://localhost:5600" # no slash
    EVENTS_URL = BASE_AW_URL + "/api/0/buckets/$BUCKET/events?limit=-1&start=$START&end=$END"
    QUERY_AW_URL = "http://localhost:5600/api/0/query/" # no slash

    QUERY = {
        "query": [
            "events  = flood(query_bucket(\"$BUCKET\"));",
            #"not_afk = flood(query_bucket(\"aw-watcher-afk_X\"));",
            #"not_afk = filter_keyvals(not_afk, \"status\", [\"not-afk\"]);",
            #"events  = filter_period_intersect(events, not_afk);",
            "title_events  = merge_events_by_keys(events, [\"app\", \"title\"]);",
            "title_events  = sort_by_duration(title_events);",
            "app_events  = merge_events_by_keys(title_events, [\"app\"]);",
            "app_events  = sort_by_duration(app_events);",
            "app_events  = limit_events(app_events, 10);",
            "title_events  = limit_events(title_events, 10);",
            "RETURN  = {\"app_events\": app_events, \"title_events\": title_events};",
            	";"
        ],
        "timeperiods": ["$START/$END"]
    }

    # TODO: optimize!
    QUERY_WEEKLY_USAGE = {
        "query": [
            "events  = flood(query_bucket(\"$BUCKET\"));",
            #"not_afk = flood(query_bucket(\"aw-watcher-afk_X\"));",
            #"not_afk = filter_keyvals(not_afk, \"status\", [\"not-afk\"]);",
            #"events  = filter_period_intersect(events, not_afk);",
            "RETURN = events;",
            	";"
        ],
        "timeperiods": ["$START/$END"]
    }


    RANKING_APP_NAME = 1
    RANKING_WIN_TITLE = 2

    def __init__(self):
        pass

    def get_ranking_from_list(self, events, ranking_type, app_name_filter = None):

        ranking = {}
        total_time = 0.0

        force_lowercase = False
        if ranking_type == self.RANKING_WIN_TITLE:
            ranking_type = "title_events"
            ranking_type_l1 = "title"
            force_lowercase = False
        else:
            ranking_type = "app_events"
            ranking_type_l1 = "app"
            force_lowercase = True

        if events != []:
            events = events[ranking_type]


        for record in events:
            total_time += record['duration']
            app_name = record['data'][ranking_type_l1]

            if(type(app_name_filter) == str and record['data']['app'].lower() != app_name_filter.lower()):
                continue

            if force_lowercase:
                app_name = app_name.lower()

            if(not app_name in ranking):
                ranking[app_name] = record['duration']
            else:
                ranking[app_name] = ranking[app_name] + record['duration']

        return (ranking, total_time)

    # todo: allow GUI/config file bucket selection
    def get_bucket_name(self):
        try:
            bucket_list = json.loads(urllib.request.urlopen(self.BASE_AW_URL + "/api/0/buckets/").read().decode())

            for bucket in list(bucket_list.keys()):
                if "aw-watcher-window" in bucket:
                    return bucket

        except:
            pass # todo: handle this

        return ""

    def get_ranking_by_date_range(self, bucket_name, start, end, query = QUERY):
        day_start = datetime(start.year, start.month, start.day)
        day_end = datetime(end.year, end.month, end.day) \
            + timedelta(days = 1) - timedelta(seconds = 1)

        day_start_str = timeutils.format_rfc3339(day_start)
        day_end_str = timeutils.format_rfc3339(day_end)

        jsondata = json.dumps(query)
        jsondata = jsondata.replace("$BUCKET", bucket_name) \
                           .replace("$START", day_start_str) \
                           .replace("$END", day_end_str)
        jsondataasbytes = jsondata.encode('utf-8')

        req = urllib.request.Request(self.QUERY_AW_URL)
        req.add_header('Content-Type', 'application/json; charset=utf-8')
        req.add_header('Content-Length', len(jsondataasbytes))

        response = urllib.request.urlopen(req, jsondataasbytes)
        events = json.loads(response.read())[0]
        return events

    def get_weekly_usage(self, bucket, day_in_week):
        weekly_events = self.get_ranking_by_date_range(bucket, timeutils.find_week_first(day_in_week), timeutils.find_week_last(day_in_week), self.QUERY_WEEKLY_USAGE)

        weekly_usage = [0, 0, 0, 0, 0, 0, 0]
        for record in weekly_events:
            # Replace Z timezone (UTC) as fromisoformat does not support it
            record_date = timeutils.parse_rfc3339(record['timestamp'])
            record_weekday = record_date.weekday()
            if record_date.day == day_in_week.day and record_date.month == day_in_week.month and record_date.year == day_in_week.year:
                pass # TODO: do routine below
            weekly_usage[record_weekday] += record['duration']

        return weekly_usage

