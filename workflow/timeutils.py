from datetime import datetime, timedelta, timezone

RFC3339_UTC_FORMAT_SECOND = "%Y-%m-%dT%H:%M:%S%z"
RFC3339_UTC_FORMAT_SUBSECOND = "%Y-%m-%dT%H:%M:%S.%f%z"
RFC3339_UTC_FORMAT_SUBSECOND_UTC = "%Y-%m-%dT%H:%M:%S.%fZ"

RFC3339_LEN_SECONDS_NAIVE = len("2020-01-01T00:00:00")
RFC3339_LEN_MILLISECONDS_NAIVE = len("2020-01-01T00:00:00.000000")

def format_rfc3339(date):
    date_utc = date.astimezone(timezone.utc)
    date_str = date.strftime(RFC3339_UTC_FORMAT_SUBSECOND_UTC)
    return date_str

def format_seconds(seconds):
    total_seconds = int(seconds)

    hours = int(seconds / 3600)
    minutes = int(seconds / 60) - 60 * hours
    seconds = total_seconds % 60

    if total_seconds < 60:
        return str(seconds) + "s"
    elif total_seconds < 3600:
        return str(minutes) + "m " + str(seconds) + "s"
    else:
        return str(hours) + "h " + str(minutes) + "m " # + str(seconds) + "s"

def parse_rfc3339(date):
    # Find where timezone part of the timestamp starts
    naive_date_len = -1
    for sep in ["Z", "+", "-"]:
        naive_date_len = date.find(sep)
        if naive_date_len >= 0:
            break
    if naive_date_len < 0:
        raise Exception("Failed to parse rfc3339 string, not timezone aware: " + date)

    # Decide whether to parse with
    # - seconds
    # - milliseconds
    # - nanoseconds but discard to milliseconds
    if naive_date_len == RFC3339_LEN_SECONDS_NAIVE:
        return datetime.strptime(date, RFC3339_UTC_FORMAT_SECOND)
    elif naive_date_len <= RFC3339_LEN_MILLISECONDS_NAIVE and naive_date_len > RFC3339_LEN_SECONDS_NAIVE:
        return datetime.strptime(date, RFC3339_UTC_FORMAT_SUBSECOND)
    elif naive_date_len > RFC3339_LEN_MILLISECONDS_NAIVE:
        # %f does not support nanosecond precision, discard to milliseconds
        to_delete = naive_date_len - RFC3339_LEN_MILLISECONDS_NAIVE
        # first half is timezone unaware, discarded to millisecond
        # second half is timezone
        date = date[0:-to_delete-1]+date[naive_date_len:]
        return datetime.strptime(date, RFC3339_UTC_FORMAT_SUBSECOND)
    else:
        raise Exception("Failed to parse rfc3339 string, unknown format: " + date)

def find_week_first(date):
    return date - timedelta(days=date.weekday())

def find_week_last(date):
    return find_week_first(date) + timedelta(days=6)
