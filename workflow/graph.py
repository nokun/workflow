from gi.repository import Gtk
import cairo

class GraphDrawingArea(Gtk.DrawingArea):

    MAX_ITEMS = 7
    COL_WIDTH = 30
    MARGIN_H = 10
    MARGIN_V = 10
    MAX_HEIGHT = 100

    # day_list example: [50, 90, 20, 5, 10, 120, 5, 100]  -> whatever time unit, scaled proportionally
    day_list = []

    def __init__(self, day_list, **kwargs):
        super().__init__(**kwargs)
        self.set_day_list(day_list)
        self.connect("draw", self.on_draw)

    def set_day_list(self,day_list):
        self.day_list = day_list

    # TODO: animate
    def on_draw(self, widget, cr):

        # relative row scaling
        max_value = -1
        for value in self.day_list:
            if value > max_value:
                max_value = value

        for i in range(0, len(self.day_list)):
            self.day_list[i] = self.day_list[i] / max_value


	    # min. reserved sizes
        total_width = self.MAX_ITEMS * (self.MARGIN_H + self.COL_WIDTH) + self.MARGIN_H
        total_height = self.MARGIN_V + self.MAX_HEIGHT


        if max_value <= 0:
            self.set_size_request(total_width,0)
            self.set_visible(False)
        else:
            self.set_size_request(total_width,total_height)
            self.set_visible(True)

        scale_h = self.get_allocated_width() / total_width

        cr.set_source_rgb(1,1,1)
        cr.rectangle(0,0,total_width * scale_h,total_height)
        cr.fill_preserve()
        cr.stroke()



        for n, value in enumerate(self.day_list):
            if n > self.MAX_ITEMS - 1:
                return

            # color formula - change for a different graph palette
            cr.set_source_rgb(0.7 + 0.08 * n, 0.1 + 0.1 * n, 0.1)
            cr.rectangle( \
                	 float((self.MARGIN_H+self.COL_WIDTH)*n + self.MARGIN_H, ) * scale_h, \
                         self.MARGIN_V + self.MAX_HEIGHT - value, \
                         int(self.COL_WIDTH * scale_h), \
                         -value * self.MAX_HEIGHT )
            cr.fill_preserve()
            cr.stroke()
        
